#! /usr/bin/python3
# -*- coding:utf-8 -*- 
# Author: vicnic 
# Date: 2018-09-17 21:36:51 
import random
import time
import requests
import json

def analyzeJson(data ,head):
    response = requests.post('https://app.jike.ruguoapp.com/1.0/users/collections/list',data=data,headers = head)
    json_response = response.content.decode()
    data = json.loads(json_response)
    data_list = data['data']
    for index in range(len(data_list)):
        pic_list = data_list[index]['pictures']
        if len(pic_list)>0:
            for dic in pic_list:
                pic_url = dic['picUrl']
                tl.downLoadFile(pic_url)
                timer = random.randint(0,3)
                time.sleep(timer)
        



if __name__ == '__main__':  
    cookie = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoiWmdsSjRxVngzVEV5V3JMM25QTExZTHE2RzlOU09zMGRWNlAya0MreDk0VEV0d0F5SkwxXC9NQThkNkdETmFMODQzS0V2cGdzNFAyNTdwc1NYMk1adFwva0VNUW9GamJqem83VUI5bTBqZGpzS2FiT044UnRPQURDaGc4YTlDNkcwa0dvXC95TGczWm05TzhcL1YrOXlwa3pmSFZHZUFNS2oxMWlwR1Q1OXJxTDRyRlM3alVWdVU1RENNb0MwUndVcVdXNWNZZUFGZ0g1UHpXVFVCeENVbG5xRTRBOXBVZm1MNWpSRlV6QnVNODZzcTVzV1BraHdjeE1uQnRjS3NldGd4MlF3SWxnWDE2cHRyXC9KdkNzMUVsRXFLN3Qrc3ZGaHlGODZEK3dRdzljVXhkbXJvXC9pRjhDUEhVdmpxMXJvOXhuNGFWUnMzNlBjWUlXOHRVMGE0M01IVHdnPT0iLCJ2IjozLCJpdiI6IkpYaEVRTGJ1R0VMV2FoTDkzeTFcLzFnPT0iLCJpYXQiOjE1MzcxOTI3MzAuODU2fQ.Zg-wm8Y-cUCtR0eKDbnyNsode1dTpz_D6Rj8CpEWXjg'
    user_agent = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'
    header = {'x-jike-access-token':cookie,'User-Agent':user_agent}
    data = {'limit':20,'categoryAlias':'RECOMMENDATION'}

    analyzeJson(data,header)